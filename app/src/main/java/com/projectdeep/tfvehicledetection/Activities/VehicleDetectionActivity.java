package com.projectdeep.tfvehicledetection.Activities;

import android.graphics.Bitmap;
import android.graphics.Rect;
import android.graphics.RectF;
import android.os.Bundle;
import android.renderscript.RenderScript;
import android.util.Log;
import android.view.Display;

import com.projectdeep.tfvehicledetection.CameraBase.CameraActivity;
import com.projectdeep.tfvehicledetection.Drawing.VehicleBorder;
import com.projectdeep.tfvehicledetection.ObjectTracking.ObjectTracking;
import com.projectdeep.tfvehicledetection.ObjectTracking.TrackingWithOpticalFlow;
import com.projectdeep.tfvehicledetection.R;
import com.projectdeep.tfvehicledetection.Utils.ImageUtils;
import com.projectdeep.tfvehicledetection.Tensorflow.Config;
import com.projectdeep.tfvehicledetection.Tensorflow.TensorFlowYoloDetector;
import com.projectdeep.tfvehicledetection.Tensorflow.Recognition;
import com.projectdeep.tfvehicledetection.Utils.imageopsRS;
import rapid.decoder.*;

import java.util.ArrayList;
import java.util.List;

public class VehicleDetectionActivity extends CameraActivity {


    RenderScript rs;

    private static final String TAG = "VehicleDetection";


    private Thread thread;

    private TensorFlowYoloDetector detector;
    int width;
    int height;

    private VehicleBorder vehicleBorder;
    private boolean inDetectionProcess;
    private boolean isModelInstalled;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        isModelInstalled = false;
        detector = new TensorFlowYoloDetector(getApplicationContext());
        isModelInstalled = true;
        isTrackingInitDone = false;
        isFirstDetectionDone = false;
        rs=RenderScript.create(this);
    }


    @Override
    public void onResume() {
        vehicleBorder = new VehicleBorder();

        inDetectionProcess = false;

        Display display = getWindowManager().getDefaultDisplay();
        android.graphics.Point size = new android.graphics.Point();
        display.getSize(size);
        width = size.x;
        height = size.y;

        super.onResume();

    }


    private Bitmap detectionImage;
    private Bitmap trackingImage;
    private List<Bitmap> templateList;
    private List<Recognition> detectionResults;
    private List<ObjectTracking> objectTrackingList;

    private boolean isTrackingInitDone;
    private boolean isFirstDetectionDone;

    @Override
    public void doAfterFrame(final Bitmap bitmap) {


        if (isModelInstalled) {
            if (!inDetectionProcess) {

                detectionImage = bitmap;
                thread = new Thread() {
                    @Override
                    public void run() {
                        inDetectionProcess = true;
                        Log.d(TAG, "In Process!");

                        //Bitmap resizedBitmap = ImageUtils.getResizedBitmap(bitmap, Config.INPUT_SIZE, Config.INPUT_SIZE, true);
                        //Bitmap resizedBitmap= imageopsRS.ResizeRS(rs,bitmap,Config.INPUT_SIZE,Config.INPUT_SIZE);
                        Bitmap resizedBitmap=BitmapDecoder.from(bitmap).scale(Config.INPUT_SIZE,Config.INPUT_SIZE).decode();
                        detectionResults = detector.recognizeImage(resizedBitmap);

                        getDrawingView().clearView();
                        for (Recognition recognition : detectionResults) {
                            getDrawingView().getCanvas().drawRect(recognition.getLocation().left * width / Config.INPUT_SIZE,
                                    recognition.getLocation().top * height / Config.INPUT_SIZE,
                                    recognition.getLocation().right * width / Config.INPUT_SIZE,
                                    recognition.getLocation().bottom * height / Config.INPUT_SIZE,
                                    vehicleBorder.getPaint());

                        }
                        Log.d(TAG, "Done!");

                        isTrackingInitDone = false;

                        trackingImage = bitmap;
                        int trackingSize = detectionResults.size();

                        objectTrackingList = new ArrayList<>();
                        templateList = new ArrayList<>();

                        float w_tracking_image = (float) trackingImage.getWidth();
                        float h_tracking_image = (float) trackingImage.getHeight();

                        float w_mult = w_tracking_image / Config.INPUT_SIZE;
                        float h_mult = h_tracking_image / Config.INPUT_SIZE;

                        Log.d("Box Size", trackingSize + "");

                        for (int i = 0; i < trackingSize; i++) {
                            Recognition recognition = detectionResults.get(i);
                            RectF rect = new RectF(recognition.getLocation().left * w_mult,
                                    recognition.getLocation().top * h_mult,
                                    recognition.getLocation().right * w_mult,
                                    recognition.getLocation().bottom * h_mult);


                            Bitmap template;
                            template = Bitmap.createBitmap(trackingImage, (int) rect.left, (int) rect.top, (int) rect.width(), (int) rect.height());
                            templateList.add(template);

                            ObjectTracking objectTracking = new TrackingWithOpticalFlow(width, height, getDrawingView());
                            objectTracking.initializeTemplateForTracking(templateList.get(i), new Rect((int) rect.left, (int) rect.top, (int) rect.right, (int) rect.bottom));

                            objectTrackingList.add(objectTracking);
                        }
                        inDetectionProcess = false;
                        isTrackingInitDone = true;


                    }
                };
                thread.start();

            } else if (isTrackingInitDone) {

                if (objectTrackingList.size() != 0) {
                    getDrawingView().clearView();
                }
                for (ObjectTracking objectTracking : objectTrackingList) {
                    Rect match_rect = objectTracking.getObjectLocation(bitmap);
                    getDrawingView().getCanvas().drawRect(match_rect, vehicleBorder.getPaint());
                    Log.d("Tracking", "Tracking...");

                }

            }
        }
    }

}
