package com.projectdeep.tfvehicledetection.ObjectTracking;

import android.graphics.Bitmap;
import android.graphics.Rect;
import android.util.Log;

import org.opencv.android.Utils;
import org.opencv.calib3d.Calib3d;
import org.opencv.core.Core;
import org.opencv.core.CvType;
import org.opencv.core.DMatch;
import org.opencv.core.KeyPoint;
import org.opencv.core.Mat;
import org.opencv.core.MatOfDMatch;
import org.opencv.core.MatOfKeyPoint;
import org.opencv.core.MatOfPoint2f;
import org.opencv.core.Point;
import org.opencv.core.Scalar;
import org.opencv.features2d.DescriptorExtractor;
import org.opencv.features2d.DescriptorMatcher;
import org.opencv.features2d.FeatureDetector;
import org.opencv.imgproc.Imgproc;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

/**
 * Created by kubilaykarapinar on 12.06.18.
 */

public class TrackingWithFeatureDetection extends ObjectTracking {
    private static final String TAG = "TrackingWithFeature";

    private Scalar RED = new Scalar(255, 0, 0);
    private Scalar GREEN = new Scalar(0, 255, 0);
    private FeatureDetector detector;
    private DescriptorExtractor descriptor;
    private DescriptorMatcher matcher;
    private Mat descriptors2, descriptors1;
    private Mat img1;
    private MatOfKeyPoint keypoints1, keypoints2;


    public TrackingWithFeatureDetection(int width, int height) {
        super(width, height);
    }

    @Override
    public Rect getObjectLocation(Bitmap bitmap) {

        Mat imageMat = new Mat();
        Utils.bitmapToMat(bitmap, imageMat);

        int w = imageMat.cols();
        int h = imageMat.rows();

        List<Point> points = new ArrayList<>();

        Imgproc.cvtColor(imageMat, imageMat, Imgproc.COLOR_RGB2GRAY);
        descriptors2 = new Mat();
        keypoints2 = new MatOfKeyPoint();
        detector.detect(imageMat, keypoints2);
        descriptor.compute(imageMat, keypoints2, descriptors2);

        // Matching
        MatOfDMatch matches = new MatOfDMatch();
        if (img1.type() == imageMat.type()) {
            matcher.match(descriptors1, descriptors2, matches);
        } else {
            return new Rect();
        }
        List<DMatch> matchesList = matches.toList();

        Double max_dist = 0.0;
        Double min_dist = 100.0;

        for (int i = 0; i < matchesList.size(); i++) {
            Double dist = (double) matchesList.get(i).distance;
            if (dist != 0) {
                if (dist < min_dist)
                    min_dist = dist;
                if (dist > max_dist)
                    max_dist = dist;
            }

        }

        matches.fromList(matchesList);


        LinkedList<DMatch> good_matches = new LinkedList<DMatch>();
        for (int i = 0; i < matchesList.size(); i++) {
            if (matchesList.get(i).distance <= (3 * (min_dist)))
                good_matches.addLast(matchesList.get(i));
        }

        Log.d(TAG, "MatchSize: " + matchesList.size());
        Log.d(TAG, "GoodMatchSize: " + good_matches.size());

        MatOfDMatch goodMatches = new MatOfDMatch();
        goodMatches.fromList(good_matches);

        for (DMatch dMatch : goodMatches.toList()) {
            org.opencv.core.Point pt2 = keypoints2.toList().get(dMatch.trainIdx).pt;
            points.add(pt2);
        }


        if (imageMat.empty() || imageMat.cols() < 1 || imageMat.rows() < 1) {
            return new Rect();
        }


        LinkedList<Point> objList = new LinkedList<>();
        LinkedList<Point> sceneList = new LinkedList<>();

        List<KeyPoint> keypoints_objectList = keypoints1.toList();
        List<KeyPoint> keypoints_sceneList = keypoints2.toList();

        for (int i = 0; i < good_matches.size(); i++) {
            objList.addLast(keypoints_objectList.get(good_matches.get(i).queryIdx).pt);
            sceneList.addLast(keypoints_sceneList.get(good_matches.get(i).trainIdx).pt);
        }

        if (good_matches.size() < 4) {
            return new Rect();
        }

        MatOfPoint2f obj = new MatOfPoint2f();
        obj.fromList(objList);

        MatOfPoint2f scene = new MatOfPoint2f();
        scene.fromList(sceneList);

        Mat hg = Calib3d.findHomography(obj, scene);

        Mat obj_corners = new Mat(4, 1, CvType.CV_32FC2);
        Mat scene_corners = new Mat(4, 1, CvType.CV_32FC2);


        obj_corners.put(0, 0, new double[]{0, 0});
        obj_corners.put(1, 0, new double[]{template.getWidth(), 0});
        obj_corners.put(2, 0, new double[]{template.getWidth(), template.getHeight()});
        obj_corners.put(3, 0, new double[]{0, template.getHeight()});


        Core.perspectiveTransform(obj_corners, scene_corners, hg);


        double[] left_top = scene_corners.get(0, 0);
        double[] right_top = scene_corners.get(1, 0);
        double[] left_bottom = scene_corners.get(2, 0);
        double[] right_bottom = scene_corners.get(3, 0);

        int left = (int) (left_bottom[0] * width / w);
        int right = (int) (right_top[0] * height / h);
        int top = (int) (left_top[1] * width / w);
        int bottom = (int) (right_bottom[1] * height / h);

        Rect match_rect = new Rect(left, top, right, bottom);
        return match_rect;
    }

    @Override
    public void initializeTemplateForTracking(Bitmap bitmap, Rect templateLocation) {
        template = bitmap;

        detector = FeatureDetector.create(FeatureDetector.ORB);
        descriptor = DescriptorExtractor.create(DescriptorExtractor.ORB);
        matcher = DescriptorMatcher.create(DescriptorMatcher.BRUTEFORCE_HAMMING);
        img1 = new Mat();

        Utils.bitmapToMat(bitmap, img1);
        Imgproc.cvtColor(img1, img1, Imgproc.COLOR_RGB2GRAY);
        img1.convertTo(img1, 0); //converting the image to match with the type of the cameras image
        descriptors1 = new Mat();
        keypoints1 = new MatOfKeyPoint();
        detector.detect(img1, keypoints1);
        descriptor.compute(img1, keypoints1, descriptors1);
    }
}
