package com.projectdeep.tfvehicledetection.Tensorflow;

/**
 * Created by kubilaykarapinar on 20.06.18.
 */

public  class Config {
    public static int INPUT_SIZE = 416;   // The input size. A square image of inputSize x inputSize is assumed.
    public static int IMAGE_MEAN = 0;   // The assumed mean of the image values.
    public static float IMAGE_STD = 255.0f;   // The assumed std of the image values.
    public static String MODEL_FILE = "model_yolo_tiny_new.pb";   // The filepath of the model GraphDef protocol buffer.
    public static String LABEL_FILE = "tiny-yolo-voc-labels.txt"; // The filepath of label file for classes.
    public static String INPUT_NAME = "input_1";    // The label of the image input node.
    public static String OUTPUT_NAME = "lambda_1/Identity"; // The label of the output node.


    public static int YOLO_BLOCK_SIZE = 32;

    public static int NUM_BOXES_PER_BLOCK = 5;
    public static int MAX_RESULTS = 10;
    public static int NUM_CLASSES = 1;
    public static float MINIMUM_CONFIDENCE_YOLO = 0.30f;


    public static double[] ANCHORS = {
            0.50, 0.40, 0.91, 0.77, 1.85, 1.56, 2.71, 3.05, 4.20, 2.82

    };

    public static String[] LABELS = {
            "car"

    };

}
